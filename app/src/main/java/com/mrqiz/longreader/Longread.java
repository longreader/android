package com.mrqiz.longreader;

import java.util.List;

public class Longread {

    private String id;
    private String title;
    private String subtitle;
    private String author;
    private List<LongreadContent> contents;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<LongreadContent> getContents() {
        return contents;
    }

    public void setContents(List<LongreadContent> contents) {
        this.contents = contents;
    }
}
