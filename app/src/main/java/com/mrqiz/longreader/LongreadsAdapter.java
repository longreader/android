package com.mrqiz.longreader;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import java.util.List;

public class LongreadsAdapter extends RecyclerView.Adapter<LongreadsAdapter.ViewHolder> {

    private final LayoutInflater inflater;
    private final List<Longread> longreads;

    LongreadsAdapter(Context context, List<Longread> repos) {
        this.longreads = repos;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public LongreadsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.feed_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LongreadsAdapter.ViewHolder holder, int position) {
        Longread longread = longreads.get(position);

        holder.titleView.setText(longread.getTitle());

        if (longread.getSubtitle() != null)
            holder.subtitleView.setText(longread.getSubtitle());
        else
            holder.subtitleView.setVisibility(View.GONE);

        holder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), LongreadActivity.class);
            intent.putExtra("id", longread.getId());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return longreads.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleView, subtitleView;
        MaterialCardView cardView;

        ViewHolder(View view){
            super(view);

            cardView = (MaterialCardView) view;
            titleView = view.findViewById(R.id.title);
            subtitleView = view.findViewById(R.id.subtitle);
        }
    }

}
