package com.mrqiz.longreader;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignInActivity extends AppCompatActivity {
    final private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.SUPABASE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    final private ApiService service = retrofit.create(ApiService.class);

    EditText usernameEditText;
    EditText passwordEditText;

    SharedPreferences authData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        authData = getSharedPreferences(SharedPreferencesParams.AUTH_PREFERENCES, Context.MODE_PRIVATE);

        usernameEditText = findViewById(R.id.usernameEditText);
        passwordEditText = findViewById(R.id.passwordEditText);

        Button signInButton = findViewById(R.id.signInButton);
        signInButton.setOnClickListener(v -> {
            signIn();
        });
    }

    private void signIn() {
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (username.isEmpty() || password.isEmpty()) {
            Snackbar.make(findViewById(R.id.signInButton), "Перероверьте логин/пароль.", Snackbar.LENGTH_SHORT).show();
            return;
        }

        JsonObject credentials = new JsonObject();
        credentials.addProperty("email", username);
        credentials.addProperty("password", password);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), credentials.toString());

        service.signIn(BuildConfig.SUPABASE_PUBLIC_KEY, "password", body)
                .enqueue(new Callback<SignInResponse>() {
                    @Override
                    public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                        if (!response.isSuccessful()) {
                            Snackbar.make(findViewById(R.id.signInButton), "Не удалось войти.", Snackbar.LENGTH_SHORT).show();
                            return;
                        }

                        SharedPreferences.Editor editor = authData.edit();
                        editor.putString(SharedPreferencesParams.AUTH_PREFERENCES_JWT_TOKEN, response.body().getAccessToken());
                        editor.putString(SharedPreferencesParams.AUTH_PREFERENCES_USER_ID, response.body().getUser().getId());
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(),  MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Snackbar.make(findViewById(R.id.signInButton), "Не удалось войти.", Snackbar.LENGTH_SHORT).show();
                    }
                });
    }
}