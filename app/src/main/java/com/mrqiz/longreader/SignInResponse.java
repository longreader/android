package com.mrqiz.longreader;

public class SignInResponse {
    private String access_token;
    private long expires_in;
    private String refresh_token;
    private String token_type;

    private SignInUserResponse user;

    public String getAccessToken() {
        return access_token;
    }

    public void setAccessToken(String access_token) {
        this.access_token = access_token;
    }

    public long getExpiresIn() {
        return expires_in;
    }

    public void setExpiresIn(long expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefreshToken() {
        return refresh_token;
    }

    public void setRefreshToken(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getTokenType() {
        return token_type;
    }

    public void setTokenType(String token_type) {
        this.token_type = token_type;
    }

    public SignInUserResponse getUser() {
        return user;
    }

    public void setUser(SignInUserResponse user) {
        this.user = user;
    }
}
