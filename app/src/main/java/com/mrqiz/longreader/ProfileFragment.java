package com.mrqiz.longreader;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileFragment extends Fragment {
    final private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.SUPABASE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    final private ApiService service = retrofit.create(ApiService.class);

    SharedPreferences authData;

    String jwtToken;
    String userId;

    LinearLayout profileProgressBar;
    LinearLayout profileContent;
    TextView nicknameTextView;
    TextView nameTextView;
    Button signOutButton;

    public ProfileFragment () {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        profileProgressBar = getActivity().findViewById(R.id.profileProgressBar);
        profileContent = getActivity().findViewById(R.id.profileContent);
        nicknameTextView = getActivity().findViewById(R.id.nicknameTextView);
        nameTextView = getActivity().findViewById(R.id.nameTextView);
        signOutButton = getActivity().findViewById(R.id.signOutButton);

        authData = getActivity().getSharedPreferences(SharedPreferencesParams.AUTH_PREFERENCES, Context.MODE_PRIVATE);

        jwtToken = authData.getString(SharedPreferencesParams.AUTH_PREFERENCES_JWT_TOKEN, "");
        userId = authData.getString(SharedPreferencesParams.AUTH_PREFERENCES_USER_ID, "");

        signOutButton.setOnClickListener(v -> signOut());

        service.getProfile(BuildConfig.SUPABASE_PUBLIC_KEY, "Bearer " + jwtToken, "eq." + userId)
            .enqueue(new Callback<List<Profile>>() {
                @Override
                public void onResponse(@NonNull Call<List<Profile>> call, @NonNull Response<List<Profile>> response) {
                    if (response.code() == 401) {
                        Toast.makeText(getActivity(), "Срок активости сессии истек, повторите вход для продолжения.", Toast.LENGTH_SHORT).show();
                        signOut();
                        return;
                    }

                    if (!response.isSuccessful()) {
                        Snackbar.make(getView(), "Не удалось загрузить профиль. (ошибка " + response.code() + ")", Snackbar.LENGTH_SHORT).show();
                        return;
                    }

                    Profile profileResponse = response.body().get(0);
                    profileProgressBar.setVisibility(View.GONE);
                    profileContent.setVisibility(View.VISIBLE);
                    signOutButton.setVisibility(View.VISIBLE);
                    nicknameTextView.setText("@" + profileResponse.getNickname());
                    nameTextView.setText(profileResponse.getName());
                }

                @Override
                public void onFailure(@NonNull Call<List<Profile>> call, @NonNull Throwable t) {
                    Snackbar.make(getView(), "Не удалось загрузить профиль.", Snackbar.LENGTH_SHORT).show();
                }
            });
    }

    private void signOut() {
        SharedPreferences.Editor editor = authData.edit();

        editor.remove(SharedPreferencesParams.AUTH_PREFERENCES_JWT_TOKEN);
        editor.remove(SharedPreferencesParams.AUTH_PREFERENCES_USER_ID);
        editor.apply();

        Intent intent = new Intent(getContext(), SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }
}