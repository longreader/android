package com.mrqiz.longreader;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import java.util.List;

public class LongreadAdapter extends RecyclerView.Adapter<LongreadAdapter.ViewHolder> {

    private final LayoutInflater inflater;
    private final List<LongreadContent> longreadContents;

    LongreadAdapter(Context context, List<LongreadContent> longreadContents) {
        this.longreadContents = longreadContents;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public LongreadAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.longread_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LongreadAdapter.ViewHolder holder, int position) {
        LongreadContent longreadContent = longreadContents.get(position);

        if (longreadContent.getType().equals("paragraph")) {
            holder.paragraphItem.setText(longreadContent.getContent());
            holder.paragraphItem.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return longreadContents.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        TextView paragraphItem;

        ViewHolder(View view){
            super(view);

            layout = (LinearLayout) view;
            paragraphItem = (TextView) view.findViewById(R.id.longread_paragraph_item);
        }
    }

}
