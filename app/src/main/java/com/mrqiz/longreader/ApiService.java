package com.mrqiz.longreader;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    @POST("auth/v1/token")
    Call<SignInResponse> signIn(@Header("ApiKey") String anonAuthorization, @Query("grant_type") String grantType, @Body() RequestBody requestBody);

    @GET("rest/v1/profiles?select=*")
    Call<List<Profile>> getProfile(@Header("ApiKey") String anonAuthorization, @Header("Authorization") String userAuthorization, @Query("id") String id);

    @GET("rest/v1/longreads?limit=100&order=created_at.desc.nullslast&select=*")
    Call<List<Longread>> getFeed(@Header("ApiKey") String anonAuthorization, @Header("Authorization") String userAuthorization);

    @GET("rest/v1/longreads?select=*")
    Call<List<Longread>> getLongread(@Header("ApiKey") String anonAuthorization, @Header("Authorization") String userAuthorization, @Query("id") String id);
}
