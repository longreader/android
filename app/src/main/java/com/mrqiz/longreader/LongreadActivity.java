package com.mrqiz.longreader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LongreadActivity extends AppCompatActivity {
    final private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.SUPABASE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    final private ApiService service = retrofit.create(ApiService.class);

    LongreadAdapter longreadAdapter;

    MaterialToolbar materialToolbar;

    TextView titleTextView;
    TextView subtitleTextView;
    RecyclerView contentsRecyclerView;

    LinearLayout longreadProgressBar;

    SharedPreferences authData;
    String jwtToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_longread);

        longreadProgressBar = findViewById(R.id.longreadProgressBar);

        titleTextView = findViewById(R.id.longreadTitleTextView);
        subtitleTextView = findViewById(R.id.longreadSubtitleTextView);
        contentsRecyclerView = findViewById(R.id.longreadContentsRecyclerView);

        materialToolbar = findViewById(R.id.longreadToolbar);
        materialToolbar.setNavigationOnClickListener(v -> finish());

        authData = getSharedPreferences(SharedPreferencesParams.AUTH_PREFERENCES, Context.MODE_PRIVATE);
        jwtToken = authData.getString(SharedPreferencesParams.AUTH_PREFERENCES_JWT_TOKEN, "");

        service.getLongread(BuildConfig.SUPABASE_PUBLIC_KEY, jwtToken, "eq." + getIntent().getStringExtra("id"))
            .enqueue(new Callback<List<Longread>>() {
                @Override
                public void onResponse(Call<List<Longread>> call, Response<List<Longread>> response) {
                    Longread longread = response.body().get(0);

                    longreadProgressBar.setVisibility(View.GONE);

                    titleTextView.setText(longread.getTitle());
                    if (longread.getSubtitle() != null) subtitleTextView.setText(longread.getSubtitle());

                    longreadAdapter = new LongreadAdapter(getApplicationContext(), longread.getContents());
                    contentsRecyclerView.setAdapter(longreadAdapter);

                    longreadAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<List<Longread>> call, Throwable t) {

                }
            });
    }
}