package com.mrqiz.longreader;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FeedFragment extends Fragment {
    final private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.SUPABASE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    final private ApiService service = retrofit.create(ApiService.class);

    SharedPreferences authData;

    RecyclerView longreadsList;

    LinearLayout feedProgressBar;

    LongreadsAdapter longreadsAdapter;

    public FeedFragment () {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feed, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        feedProgressBar = getView().findViewById(R.id.feedProgressBar);

        authData = getActivity().getSharedPreferences(SharedPreferencesParams.AUTH_PREFERENCES, Context.MODE_PRIVATE);

        String jwtToken = authData.getString(SharedPreferencesParams.AUTH_PREFERENCES_JWT_TOKEN, "");

        service.getFeed(BuildConfig.SUPABASE_PUBLIC_KEY, jwtToken)
            .enqueue(new Callback<List<Longread>>() {
                @Override
                public void onResponse(Call<List<Longread>> call, Response<List<Longread>> response) {
                    assert response.body() != null;

                    feedProgressBar.setVisibility(View.GONE);

                    longreadsList = getView().findViewById(R.id.longreadsList);
                    longreadsAdapter = new LongreadsAdapter(getContext(), response.body());
                    longreadsList.setAdapter(longreadsAdapter);

                    longreadsAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<List<Longread>> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
    }
}
