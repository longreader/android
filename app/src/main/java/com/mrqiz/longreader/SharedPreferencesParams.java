package com.mrqiz.longreader;

public class SharedPreferencesParams {
    public static final String AUTH_PREFERENCES = "auth";
    public static final String AUTH_PREFERENCES_JWT_TOKEN = "jwtToken";
    public static final String AUTH_PREFERENCES_USER_ID = "userId";
//    public static final String APP_AUTH = "mysettings";
}
