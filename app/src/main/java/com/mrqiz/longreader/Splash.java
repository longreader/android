package com.mrqiz.longreader;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Splash extends AppCompatActivity {
    final private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.SUPABASE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    final private ApiService service = retrofit.create(ApiService.class);

    SharedPreferences authData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        authData = getSharedPreferences(SharedPreferencesParams.AUTH_PREFERENCES, Context.MODE_PRIVATE);

        String jwtToken = authData.getString(SharedPreferencesParams.AUTH_PREFERENCES_JWT_TOKEN, "");
        String userId = authData.getString(SharedPreferencesParams.AUTH_PREFERENCES_USER_ID, "");

        Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (jwtToken.isEmpty() || userId.isEmpty()) {
                Intent i = new Intent(this, SignInActivity.class);
                startActivity(i);
                return;
            }

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }, 2000);
    }
}